package ru.t1.tbobkov.tm;

import ru.t1.tbobkov.tm.constant.ArgumentConstant;
import ru.t1.tbobkov.tm.constant.CommandConstant;
import ru.t1.tbobkov.tm.model.Command;
import ru.t1.tbobkov.tm.repository.CommandRepository;
import ru.t1.tbobkov.tm.api.ICommandRepository;
import ru.t1.tbobkov.tm.util.TerminalUtil;

import static ru.t1.tbobkov.tm.util.FormatUtil.formatBytes;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArgs(args);
        processCommands();
    }

    private static void processArgs(final String[] args) {
        if (args == null || args.length < 1) return;
        processArg(args[0]);
        exit();
    }

    private static void processCommands() {
        System.out.println("*** TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.printf("\n");
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void showVersion() {
        System.out.println("[version]");
        System.out.println("1.8.0");
    }

    private static void showAbout() {
        System.out.println("[about]");
        System.out.println("Name: Timur Bobkov");
        System.out.println("E-mail: tbobkov@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[help]");
        for (Command command: COMMAND_REPOSITORY.getCommands()) System.out.println(command);
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processorsCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorsCount);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("MAXIMUM MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    private static void showArgumentError() {
        System.out.println("[error]");
        System.out.println("incorrect argument, use '-h' to see the list of commands and arguments");
        System.exit(1);
    }

    private static void showCommandError() {
        System.out.println("[error]");
        System.out.println("incorrect command, type 'help' to see the list of commands and arguments");
    }

    private static void processArg(final String arg) {
        switch (arg.toLowerCase()) {
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            case ArgumentConstant.INFO:
                showSystemInfo();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand(final String command) {
        switch (command.toLowerCase()) {
            case CommandConstant.VERSION:
                showVersion();
                break;
            case CommandConstant.ABOUT:
                showAbout();
                break;
            case CommandConstant.HELP:
                showHelp();
                break;
            case CommandConstant.INFO:
                showSystemInfo();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

}