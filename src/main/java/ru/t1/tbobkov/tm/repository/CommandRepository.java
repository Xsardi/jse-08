package ru.t1.tbobkov.tm.repository;

import ru.t1.tbobkov.tm.api.ICommandRepository;
import ru.t1.tbobkov.tm.constant.ArgumentConstant;
import ru.t1.tbobkov.tm.constant.CommandConstant;
import ru.t1.tbobkov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandConstant.HELP, ArgumentConstant.HELP, "show this list of commands");

    public static final Command VERSION = new Command(CommandConstant.VERSION, ArgumentConstant.VERSION, "show version info");

    public static final Command ABOUT = new Command(CommandConstant.ABOUT, ArgumentConstant.ABOUT, "show developer info");

    public static final Command INFO = new Command(CommandConstant.INFO, ArgumentConstant.INFO, "show system info");

    public static final Command EXIT = new Command(CommandConstant.EXIT, null, "close application");

    public static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
